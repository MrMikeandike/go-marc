# go-marc -- because terminals can be stressful
go-marc allows you to slowly read through *The Meditations* by Marcus Aurelius in your terminal.
It primarly achieves this by showing snippets at logon.

# TODO:
Literally everything

# Possible features

The features listed here are ones that I think could improve the app, but am still on the fence on.

* Because some of the snippets get quite long, I think its a good idea to let the user tell marc when they are done reading a specific section. I am thinking this will be done by telling the user to run "marc --read" or something similar, and next time it will show the next snippet.

* following the above, having "marc --unread <number>", "marc --previous <number>", "marc --next <number>", "marc --next-unread"

* a "marc list --unread", "marc list --read", "marc toc", "marc progress --percent"

# Credits

* http://classics.mit.edu/Antoninus/meditations.html for the txt of the meditations at http://classics.mit.edu/Antoninus/meditations.mb.txt