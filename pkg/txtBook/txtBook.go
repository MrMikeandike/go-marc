package txtBook

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// Parse takes a filepath that is assumed to be meditations.mb.txt from http://classics.mit.edu/Antoninus/meditations.mb.txt
// and outputs a parsed version in YAML format
func Parse(inFP string, outFP string) error {
	f, err := os.Open(inFP)
	if err != nil {
		return err
	}
	chBytes, err := splitBook(f)
	if err != nil {
		return err
	}
	if err = f.Close(); err != nil {
		return err
	}
	i := 0
	for _, ch := range chBytes[0:2] {
		fmt.Printf("%d\n'''%s'''\n\n", i, string(ch))
	}

	_ = outFP
	return nil
}

func splitBook(r io.Reader) ([][]byte, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	chapters := bytes.Split(b, []byte("\n\n----------------------------------------------------------------------\n\n"))
	return chapters[1 : len(chapters)-1], nil

}
