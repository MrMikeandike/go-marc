package main

import (
	"log"
	"os"

	"gitlab.com/MrMikeandike/go-marc/pkg/txtBook"
)

func main() {
	err := txtBook.Parse(os.Getenv("HOME")+"/go/go-marc/meditations.mb.txt", "")
	if err != nil {
		log.Fatal(err)
	}

}
