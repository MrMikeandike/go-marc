.PHONY := deb_build go_build clean_go_build
MARC_DEB_VERSION := "1.0"
CONTROL_TEMPLATE := "Package: go-marc\nVersion: $(MARC_DEB_VERSION)\nSection: custom \nPriority: optional\nArchitecture: all\nEssential: no\nInstalled-Size: 1024\nMaintainer: gitlab.com/MrMikeandike/go-marc\nDescription: Eventually print marcus Aurelius quotes"

deb_build: ## builds deb package
	@mkdir -p ./deb/go-marc/DEBIAN
	@chmod -R 0775 ./deb/go-marc/DEBIAN
	@mkdir -p ./deb/go-marc/usr/bin
	@echo $(CONTROL_TEMPLATE) > ./deb/go-marc/DEBIAN/control
	@make go_build
	@cp ./builds/marc ./deb/go-marc/usr/bin/
	@cd ./deb && dpkg-deb --build go-marc
	@mv ./deb/go-marc.deb ./builds/go-marc-$(MARC_DEB_VERSION)_amd64.deb
	@make clean_go_build

go_build: ## builds go package
	@go build ./cmd/marc
	@mv ./marc ./builds/ 
clean_go_build: ## removes binary and stuff after go build
	@rm ./marc
